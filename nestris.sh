#!/bin/sh
#Nestris Launch/Setup Script

name="Nestris"
Game=Nestris
game=nestris
configdir="/home/$USER/.config/Nestris"
binary=nestris
image_path="/usr/share/games/Nestris/$game.png"
icon_path="/usr/share/games/Nestris/$game.png"
 
if [ -d "$configdir" ]; then
  ### Take action if $configdir exists ###
   echo "Game files exists, skipping setup and launching game.."
else
  ###  Control will jump here if $configdir does NOT exists ###
  echo "Welcome to the $name setup, we will begin setting the game up for you."
  yad --width=750 --height=100 --info --title="$name Installation" --window-icon="$icon_path" --image="$image_path" --text="Welcome to the $name setup, we will begin setting the game up for you." --button="OK:1" --button="Cancel:0"
  if [ $? -eq 0 ]; then
  echo "Script exited by user"
  exit 0
fi
(
echo "10" ; sleep 1
echo "# Setting game files" ; sleep 1
echo "50" ; sleep 1
echo "# Creating config folder" ; sleep 1
mkdir $configdir
echo "90" ; sleep 1
echo "# Creating symlink the Nestris binary.." ; sleep 1
ln -s /usr/share/games/Nestris/nestris "$configdir"
echo "100" ; sleep 1
) |
yad --progress \
  --width=550 \
  --height=100 \
  --title="Setting up $name" \
  --text="Preparing to setup game..." \
   --window-icon="$icon_path" \
  --percentage=0 \
  --auto-close

if [ "$?" = -1 ] ; then
        yad --error \
          --text="Update cancelled."
fi
fi

echo "Starting game"
cd $configdir
./$binary
